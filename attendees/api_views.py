from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee
from events.models import Conference, Location
from django.views.decorators.http import require_http_methods
import json

class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name", "email"]

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = ["name", "email", "company_name","created"]

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    else:
        content = json.loads(request.body)

    # Get the Conference object and put it in the content dict
    try:
        conference = Conference.objects.get(id=conference_id)
        content["conference"] = conference
    except Conference.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        location = Attendee.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)

    Attendee.objects.filter(id=id).update(**content)

    # copied from get detail
    location = Attendee.objects.get(id=id)
    return JsonResponse(
        location,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )
