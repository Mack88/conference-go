from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo_url, get_weather_data


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]

@require_http_methods(["GET"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)

    # Get the Location object and put it in the content dict
    try:
        location = Location.objects.get(id=content["location"])
        content["location"] = location
    except Location.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid location id"},
            status=400,
        )

    conference = Conference.objects.create(**content)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False,
    )

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url"
    ]

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
         "description",
         "max_presentations",
         "max_attendees",
         "starts",
         "ends",
         "created",
         "updated",
         "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }
    def get_extra_data(self, o):
        return {"weather": get_weather_data(o.location.city, o.location.state.name)}


@require_http_methods({"GET", "POST", "DELETE"})
def api_show_conference(request, id):
    if request.method == "GET":
        try:

            conference = Conference.objects.get(id=id)
            # city = conference.location.city
            # state = conference.location.state
            # weather_data = get_weather_data(city, state)
            # if weather_data is None:
            #     response_data = {"conference": conference}
            # else:
            #     response_data = {
                # "conference": conference,
                # "weather": weather_data,
        #}
            return JsonResponse(
                conference,
                encoder=ConferenceDetailEncoder,
                safe=False
        )
        except Conference.DoesNotExist:
            return JsonResponse({"message": "Invalid conference id"}, status=400)

    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,safe=False,
    )
    elif request.method == "DELETE":
        try:
            conference = Conference.objects.get(id=id)
            conference.delete()
            return JsonResponse({"message": "Conference deleted successfuly"})
        except Conference.DoesNotExist:
            return JsonResponse({"message": "Invalid conference id"}, status=400)


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
            safe=False
    )
    else:
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
        {"message": "Invalid state abbreviation"},
        status=400,
        )

        state_full_name = state.name
        print(state_full_name)

        picture_url = get_photo_url(f'{content["city"]} {state_full_name}')
        print(picture_url)
        content["picture_url"] = picture_url

        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False

        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
    # copied from create
        content = json.loads(request.body)
    try:
        # new code
        if "state" in content:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
    except State.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid state abbreviation"},
            status=400,
        )

    Location.objects.filter(id=id).update(**content)

    # copied from get detail
    location = Location.objects.get(id=id)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )
