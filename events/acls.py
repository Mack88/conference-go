import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY





def get_photo_url(query):
    """
    Get the URL of a picture from the Pexels API.
    """
    url = f"https://api.pexels.com/v1/search?query={query}"

    headers = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, headers=headers)
    api_dict = response.json()
    return api_dict['photos'][0]['src']['original']


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    geocoding_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(geocoding_url)
    api_dict = response.json()
    print(api_dict)
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response
    try:
        lat = api_dict[0]["lat"]
        lon = api_dict[0]["lon"]
    except:
        return None

    # Create the URL for the current weather API with the latitude
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(weather_url)
    weather_dict = weather_response.json()

    main_temp = weather_dict["main"]["temp"]
    weather_description = weather_dict["weather"][0]["description"]

    weather = {
        "temperature": main_temp,
        "description": weather_description,
    }
    return weather

    # Return the dictionary
